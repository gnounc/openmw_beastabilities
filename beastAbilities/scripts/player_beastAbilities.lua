local math = require('math')
local ui = require('openmw.ui')
local self = require('openmw.self')
local core = require('openmw.core')
local types = require('openmw.types')
local input = require('openmw.input')
local gameTime = require('openmw_aux.time')


local time = 0
local dbg = false

local init = false
local hasStats = false
local playerRace = ""
local nightEye = false
local waterBreathing = false
local prevCell = ""


if not types.Actor.activeEffects then
	error("OpenMW version out of date.")
end

local function d_message(msg)
	if not dbg then return end

	ui.showMessage(msg)
end

local function d_print(fname, msg)
	if not dbg then return end

	if fname == nil then
		fname = "\x1b[35mnil"
	end

	if msg == nil then
		msg = "\x1b[35mnil"
	end

	print("\n\t\x1b[33;3m" .. fname .. "\n\t\t\x1b[33;3m" .. msg .. "\n\x1b[39m")
end

local function formatTime(time)
	return tostring(time.hour) .. ":" .. tostring(time.minute) .. ":" .. tostring(time.second)
end

local function getTime()
	local gt = core.getGameTime()
	local t_day = math.floor(gt / gameTime.day)
	local t_hour = math.floor((gt % gameTime.day) / gameTime.hour )
	local t_minute = math.floor((gt % gameTime.hour) / gameTime.minute)
	local t_second = math.floor((gt % gameTime.minute) / gameTime.second)

	local t = {day = t_day, hour = t_hour, minute = t_minute, second = t_second}
	local s = formatTime(t)

	t.timestring = s

	return t
end

local function upateWaterBreathing()
	if playerRace ~= "argonian" then return end

	local isTemple = self.cell.name == "Vivec, Puzzle Canal, Center"

	if prevCell ~= self.cell.name then
		d_print("updateWaterBreathing-place", self.cell.name)
		d_message("new place: " .. self.cell.name)
	end

	if isTemple == true and waterBreathing then
		d_print("updateWaterBreathing", "turn waterbreathing off")
		--turn waterbreathing off here
		local spell = core.magic.spells["gnounc_aqualung"]
		types.Actor.spells(self):remove(spell)
		waterBreathing = false
	end

	if isTemple == false and not waterBreathing then
		d_print("updateWaterBreathing", "turn waterbreathing on")
		--turn waterbreathing off here
		local spell = core.magic.spells["gnounc_aqualung"]
		types.Actor.spells(self):add(spell)
		waterBreathing = true
	end

	prevCell = self.cell.name
end

local function endEffects()
	local spell = core.magic.spells["gnounc_" .. playerRace]
	types.Actor.spells(self):remove(spell)
end

local function addBeastAbilities()
	if playerRace == "khajiit" then
		types.Actor.spells(self):remove("eye of night")
		types.Actor.spells(self):add("gnounc_cateye")
		types.Actor.spells(self):add("gnounc_furcoat")
		types.Actor.spells(self):add("gnounc_catlike_reflexes")
	elseif playerRace == "argonian" then
		types.Actor.spells(self):add("gnounc_argonian_scales")
		types.Actor.spells(self):add("gnounc_natural_swimmer")
	end
end

local function event_getRace(race)
	playerRace = race

	d_message("event_getRace: " .. race)

	if playerRace == "khajiit" or playerRace == "argonian" then
		addBeastAbilities()
	end
end

local function onUpdate(dt)
	time = time + dt

	if init == false then
		local hasstats = input.getControlSwitch(input.CONTROL_SWITCH.ViewMode) --thanks johnnyhostile
		if hasstats then
			core.sendGlobalEvent("getPlayerRace", self)
			init = true
		end
	end

	upateWaterBreathing()
end


return {
	eventHandlers = { event_getRace = event_getRace },
	engineHandlers = { onUpdate = onUpdate } 
}

